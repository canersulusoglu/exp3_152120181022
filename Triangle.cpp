/*
 * Triangle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Triangle.h"
#include <exception>
#include <iostream>
 /**
 * Triangle constructor.
 * @param double a --> is a length of edge.
 * @param double b --> is a length of edge.
 * @param double c --> is a length of edge.
 */
Triangle::Triangle(double a, double b, double c) {
	if (a < 0 || b < 0 || c < 0) {
		throw std::invalid_argument("Length can not be negative number.");
	}
	else {
		setA(a);
		setB(b);
		setC(c);
	}
}

Triangle::~Triangle() {

}
/**
* This method sets length of edge.
* @param double a --> is a length of edge.
*/
void Triangle::setA(double a){
	if (a < 0) {
		throw std::invalid_argument("Length can not be negative number.");
	}
	else {
		this->a = a;
	}
}
/**
* This method sets length of edge.
* @param double b --> is a length of edge.
*/
void Triangle::setB(double b){
	if (b < 0) {
		throw std::invalid_argument("Length can not be negative number.");
	}
	else {
		this->b = b;
	}
}
/**
* This method sets length of edge.
* @param double c --> is a length of edge.
*/
void Triangle::setC(double c){
	if (c < 0) {
		throw std::invalid_argument("Length can not be negative number.");
	}
	else {
		this->c = c;
	}
}
/**
* This method calculates and returns circumference of triangle.
*/
double Triangle::calculateCircumference() const {
	return (this->a + this->b + this->c); 
}

