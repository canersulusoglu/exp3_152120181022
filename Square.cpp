/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Square.h"
#include <exception>
#include <iostream>
 /**
 * Square constructor.
 * @param double a --> is a length of edge.
 */
Square::Square(double a) {
	if (a < 0) {
		throw std::invalid_argument("Length can not be negative number.");
	}else {
		setA(a);
		setB(a);
	}
}

Square::~Square() {
}
/**
* This method sets length of edge.
* @param double a --> is a length of edge.
*/
void Square::setA(double a){
	if (a < 0) {
		throw std::invalid_argument("Length can not be negative number.");
	}
	else {
		this->a = a;
	}
}
/**
* This method sets length of edge.
* @param double b --> is a length of edge.
*/
void Square::setB(double b){
	if (b < 0) {
		throw std::invalid_argument("Length can not be negative number.");
	}
	else {
		this->a = b;
	}
}
/**
* This method calculates and returns circumference of square.
*/
double Square::calculateCircumference() const {
	return (this->a * 4);
}
/**
* This method calculates and returns area of square.
*/
double Square::calculateArea() const {
	return (this->a * this->a);
}
