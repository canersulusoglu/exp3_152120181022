/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"
#include <exception>
#include <iostream>
 /**
 * Circle constructor.
 * @param double r --> is a radius.
 */
Circle::Circle(double r) {
	if (r < 0) {
		throw std::invalid_argument("Radius can not be negative number.");
	}
	else {
		setR(r);
	}
}

Circle::~Circle() {
}
/**
* This method sets radius of circle.
* @param double r --> is a radius.
*/
void Circle::setR(double r) {
	if (r < 0) {
		throw std::invalid_argument("Radius can not be negative number.");
	}
	else {
		this->r = r;
	}
}
/**
* This method sets radius of circle.
* @param int r --> is a radius.
*/
void Circle::setR(int r) {
	if (r < 0) {
		throw std::invalid_argument("Radius can not be negative number.");
	}
	else {
		this->r = r;
	}
}
/**
* This method gets radius of circle.
*/
double Circle::getR() const {
	return this->r;
}
/**
* This method calculates and returns circumference of circle.
*/
double Circle::calculateCircumference() const {
	return 2 * this->PI * this->r;
}
/**
* This method calculates and returns area of circle.
*/
double Circle::calculateArea() const {
	return this->PI * this->r * this->r;
}
/**
* This operator compare two circles by their radius.
*/
bool Circle::operator==(Circle& other) const{
	if (this->r == other.r) {
		return true;
	}
	else {
		return false;
	}
}
